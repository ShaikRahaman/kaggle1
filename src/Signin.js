import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,FormGroup,Label,Input } from 'reactstrap';

function Signin(args) {
  const [modal, setModal] = useState(false);
  const [formdata,setFormdata] = useState({
    email:'',
    password:''
  })

  const handleInput = (e)=>{
    const {name,value} = e.target
    setFormdata({
      ...formdata,

      [name]:value
    })
  }
console.log(formdata)


  const toggle = (e) => 
  {
    e.preventDefault();
    setModal(!modal);}

  return (
    <>
    <div>
      <Button color="danger" onClick={toggle}>
        Signin
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
        <FormGroup>
    <Label
      for="exampleEmail"
      hidden
    >
      Email
    </Label>
    <Input
      id="exampleEmail"
      name="email"
      placeholder="Email"
      type="email"
      />
      </FormGroup>
      <FormGroup>
    <Label
      for="exampleEmail"
      hidden
    >
      Password
    </Label>
    <Input
      id="exampleEmail"
      name="Password"
      placeholder="Password"
      type="Password"
      />
      </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color='danger' onClick={toggle}> Submit</Button>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
    
    </>
  );
}

export default Signin;