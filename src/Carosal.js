import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,Card,CardTitle,CardText,Button,Col, Row,CardImg
} from 'reactstrap';

const items = [
  {
    src: 'https://www.kaggle.com/static/images/home/logged-out/hero-illo@3x.png',
     width: "100%",
    altText: 'Slide 1',
    caption: 'Slide 1',
    key: 1,

  },
  {
    src: 'https://tse2.mm.bing.net/th?id=OIP.4v9ScXSxPHzDMQ4GWnPdpwHaDe&pid=Api&P=0&h=180',
    altText: 'Slide 2',
    caption: 'Slide 2',
    key: 2,
  },
  {
    src: 'https://i.ytimg.com/vi/_uwucNViakk/maxresdefault.jpg',
    altText: 'Slide 3',
    caption: 'Slide 3',
    key: 3,
  },
];

function Carosal(args) {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  const slides = items.map((item) => {
    return (
      
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} width={"500px"}/>
        <CarouselCaption
          captionText={item.caption}
          captionHeader={item.caption}
        />
      </CarouselItem>
    );
  });

  return (
    <>
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
      {...args}
    >
      <CarouselIndicators
        items={items}
        activeIndex={activeIndex}
        onClickHandler={goToIndex}
      />
      {slides}
      <CarouselControl
        direction="prev"
        directionText="Previous"
        onClickHandler={previous}
      />
      <CarouselControl
        direction="next"
        directionText="Next"
        onClickHandler={next}
      />
    </Carousel>

    <div>
      <p><h1 >Level up with the largest AI & ML community</h1>
Join over 15M+ machine learners to share, stress test, and stay up-to-date on all the latest ML techniques and technologies. Discover a huge repository of community-published models, data & code for your next project.</p>
    </div>
    <div style = {{display:'inline-flex'}} >
      <Row sm="my-6" >
    <Card body class="cards"  >
    <CardImg 
      alt="Card image cap"
      src="https://www.kaggle.com/static/images/home/logged-out/learners-illo.svg"
      top
      height={'250px'}
    />
      <CardTitle tag="h5">
      <h1>Who's on Kaggle?</h1>
      </CardTitle>
      <CardText>
     <h2> Learners</h2>
Dive into Kaggle courses, competitions & forums.
      </CardText>
      <Button>
      Learn Something
      </Button>
    </Card>
    </Row>
    <Col sm='6'>
    <Card body>
    <CardImg
      alt="Card image cap"
      src="https://www.kaggle.com/static/images/home/logged-out/developers-illo.svg"
      top
      height={"250px"}
    />
      <CardTitle tag="h5">
      <h2>Researchers</h2>
      </CardTitle>
      <CardText>
      Advance ML with our pre-trained model hub & competitions.
      </CardText>
      <Button>
       Register
      
      </Button>
    </Card>
    <Card body>
    <CardImg
      alt="Card image cap"
      src="https://www.kaggle.com/static/images/home/logged-out/researchers-illo.svg"
      top
    height={"250px"}  
    />
      <CardTitle tag="h5">
      <h2>Developers</h2>
      </CardTitle>
      <CardText>
      Leverage Kaggle's models, notebooks & datasets.
      </CardText>
      <Button>
        Go somethings
      </Button>
    </Card>
    </Col>
    </div>
    
    </>
  );
}

export default Carosal;