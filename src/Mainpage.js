import React from 'react'
import{ Nav,NavItem,NavLink} from 'reactstrap'
import Signin from './Signin'
import Register from './Register'

function mainpage() {
  return (
    <>
    <div>

<Nav pills >
  <NavItem>
    <NavLink
      active
      href="#"
    >
      <h2>KAGGLE</h2>
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink href="#">
    Competitions
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink href="#">
    Datasets
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink
      disabled
      href="#"
    >
      Models
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink
      disabled
      href="#"
    >
      Code
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink
      disabled
      href="#"
    >
      Discussions
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink
      disabled
      href="#"
    >
     Courses

    </NavLink>
  </NavItem>
<NavItem>
    <input type="text" placeholder="search"></input>
  </NavItem>
  <Signin />
  <Register />
</Nav>
    </div>
<div>

 </div>
    </>

  )
}

export default mainpage;