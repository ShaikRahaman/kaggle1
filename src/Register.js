import axios from 'axios';
import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody,  Form, FormGroup, Label, Input,Row,Col } from 'reactstrap';

function Register(args) {
  const [modal, setModal] = useState(false);
  const [formdata,setFormdata] = useState({
    email:'',
    password:'',
    address:'',
    address2:'',
    city:'',
    state:'',
    zip:''
  })

  const handleInput = (e)=>{
    const {name,value} = e.target
    setFormdata({
      ...formdata,

      [name]:value
    })
  }

console.log(formdata)
 

  const toggle = (e) =>{ 
    e.preventDefault();
    setModal(!modal);}
    return (
        <div>
          <Button color="danger" onClick={toggle}>
            Register here
          </Button>
          <Modal isOpen={modal} toggle={toggle} {...args}>
            <ModalHeader toggle={toggle}>Modal title</ModalHeader>
            <ModalBody>
            <Form >
      <Row>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleEmail"
             hidden>
              Email 
            </Label>
            <Input
              id="exampleEmail"
              name="email"
              placeholder="Email"
              type="email"
              
            />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="examplePassword">
              Password
            </Label>
            <Input
              id="examplePassword"
              name="password"
              placeholder="password "
              type="password"
              pattern=" ^(?=.*\d) (?=.*[a-z]) (?=.*[A-Z]){3,8}$ "
             
            />
          </FormGroup>
        </Col>
      </Row>
      <FormGroup>
        <Label for="exampleAddress">
          Address
        </Label>
        <Input
          id="exampleAddress"
          name="address"
          placeholder="1234 Main St"
          
        />
      </FormGroup>
      <FormGroup>
        <Label for="exampleAddress2">
          Address 2
        </Label>
        <Input
          id="exampleAddress2"
          name="address2"
          placeholder="Apartment, studio, or floor"
         
        />
      </FormGroup>
      <Row>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleCity">
              City
            </Label>
            <Input
              id="exampleCity"
              name="city"
              
            />
          </FormGroup>
        </Col>
        <Col md={4}>
          <FormGroup>
            <Label for="exampleState">
              State
            </Label>
            <Input
              id="exampleState"
              name="state"
           
            />
          </FormGroup>
        </Col>
        <Col md={2}>
          <FormGroup>
            <Label for="exampleZip">
              Zip
            </Label>
            <Input
              id="exampleZip"
              name="zip"
             
            />
          </FormGroup>
        </Col>
      </Row>
      <FormGroup check>
        <Input
          id="exampleCheck"
          name="check"
          type="checkbox"
        />
        <Label
          check
          for="exampleCheck"
        >
          Check me out
        </Label>
      </FormGroup>
      <Button type='submit'>
        Sign in
      </Button>
    </Form>
    
            </ModalBody>
            
          </Modal>
        </div>
      );
    }
    
    
    export default Register;